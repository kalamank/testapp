<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form action="workshop1_view.php" method="POST">
    First name : <input type="text" id="firstname" name="firstname" required minlength=2>
    Last name : <input type="text" id="lastname" name="lastname" required minlength=3> 
    <button type="submit">View</button>
</form>
<div></div>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>




<script>
    $(function () { 
        //
        $('form').submit(function (e) { 
            e.preventDefault();
            if(!validation()){
                return false;
            }

            alert($(this).serialize());
            $.ajax({
                type: "GET",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                //dataType: "dataType",
                success: function (response) {
                    $('div').html(response);
                    $('form').hide();  //ซ่อน form
                    //alert(response);
                }
            });
          //  e.preventDefault();
           
        });
     });
     function validation() {
        var v_firstname =  $('#firstname');
           var v_lastname =  $('#lastname'); 
           if(v_firstname.val()==''){
               alert('Please input Fname');
               v_firstname.focus();
               return false;
           }
           if(v_lastname.val()==''){
               alert('Please input lname');
               v_lastname.focus();
               return false;
           }
           return true;
         
     }
  </script>


    
</body>
</html>